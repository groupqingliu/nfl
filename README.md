# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
>This is our project for NFL.
>
> >We are focus on Richard Sherman.
>
>This is the first version.

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
> ## We are going to follow the first six week schedule for the project
>
> >1. We need to figure out how to make the timeline work.
>
> > we are going to use the template provided in the authoring class and change the "click" into "mouseover". Restyle the lightbox. Create another JS file to store the content of the lightbox.
>
> >2. The sheild has been created and unloaded to the branch.
>
> >3. The player skin has been created and tested.


* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###
>The code of the player skin has been created.
>HTML
~~~~
<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>index</title>
    <link rel="stylesheet" href="css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Dosis:300,400,500" rel="stylesheet">
  </head>
  <body id="indexBody">
  	<div id="siteContainer">
      <section class="video">
        	<h2 id="section1" class="hidden">VIDEO</h2>
                <video poster="image/poster.jpg" id="myVideo">
                    <source src="video/trailer30.mp4" type="video/mp4"/>
                    <source src="video/trailer30.ogv" type="video/ogg"/>
                    <source src="video/trailer30.webm" type="video/webm"/>
                    Your browser does not support the video tag.
                </video>

                <div id="videobar">
                	<div id="playbutton"><img src="image/play.png" alt="play" id="play"></div>
                    <input id="seekbar" type="range" min="0" max="100" value="10" step="1">
                    <div id="videotime"><span id="curtimetext">0:00</span> / <span id="durtimetext">0:00</span></div>
                    <div id="mutebutton"><img src="image/muteon.png" alt="muteon" id="muteon" width="20px" height="22px"></div>
                    <input id="volumebar" type="range" min="0" max="100" value="100" step="1">
                    <div id="fullscrbutton"><img src="image/full.png" alt="full" id="full"></div>
                </div>
      </section>
	  </div>
    <script src="js/main.js"></script>
  </body>
</html>
~~~~
>
>JS
~~~~
(function() {

  var video, seekbar, curtimetext, durtimetext, mutebutton, volumebar, fullscrbutton;

   function ini () {
	  var playbutton = document.querySelector("#playbutton");
      video = document.querySelector("#myVideo");
	  seekbar = document.querySelector("#seekbar");
	  curtimetext = document.querySelector("#curtimetext");
	  durtimetext = document.querySelector("#durtimetext");
	  mutebutton = document.querySelector("#mutebutton");
	  volumebar = document.querySelector("#volumebar");
	  fullscrbutton = document.querySelector("#fullscrbutton");


	  playbutton.addEventListener("click", playvideo, false);
	  seekbar.addEventListener("change", videoseek, false);
	  video.addEventListener("timeupdate", videotimeupdate, false);
	  mutebutton.addEventListener("click", videomute, false);
	  volumebar.addEventListener("change", setvolume, false);
	  fullscrbutton.addEventListener("click", fullScreen, false);
  }

  function playvideo () {
	  var img = document.querySelector("#play");
  	if(video.paused){
			video.play();
			//toggleButton.innerHTML = "pause";
			img.src = "image/"+"pause"+".png";
		}
		else{
			video.pause();
			//toggleButton.innerHTML = "play";
			img.src = "image/"+"play"+".png";
		}
  }

  function videoseek () {
  	var seekto = video.duration * (seekbar.value / 100);
	video.currentTime = seekto;//to find the frame at the videobar wherever you drag to
  }

  function videotimeupdate () {
  	var nt = video.currentTime * (100 / video.duration);
	seekbar.value = nt;
	var curmins = Math.floor(video.currentTime / 60);
	var cursecs = Math.floor(video.currentTime - curmins * 60);
	var durmins = Math.floor(video.duration / 60);
	var dursecs = Math.floor(video.duration - durmins * 60);
	if(cursecs < 10) {
		cursecs = "0"+cursecs;
	}
	if(dursecs < 10) {
		dursecs = "0"+dursecs;
	}
	curtimetext.innerHTML = curmins+":"+cursecs;
	durtimetext.innerHTML = durmins+":"+dursecs;
  }//to update the time

  function videomute () {
	var muteimg = document.querySelector("#muteon");
  	if(video.muted){
			video.muted = false;
			//toggleButton.innerHTML = "pause";
			muteimg.src = "image/"+"muteon"+".png";
		}
		else{
			video.muted = true;
			//toggleButton.innerHTML = "play";
			muteimg.src = "image/"+"muteoff"+".png";
		}
  }//to control the volume and change the picture

  function setvolume () {
  	video.volume = volumebar.value / 100;
  }//to set the percentage of the volume

  function fullScreen () {
  	if(video.requestFullScreen) {
		video.requestFullScreen ();
	} else if (video.webkitRequestFullScreen) {
		video.webkitRequestFullScreen ();
	} else if (video.mozRequestFullScreen) {
		video.mozRequestFullScreen ();
	}
  }//to add the fullscreen function

  window.addEventListener("load", ini, false);





})();
~~~~
>Everyone can download and test this player skin and the function. If you have some ideas, you are welcome to rewrite the code.


* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
>Professors are welcome leave comments and advice.

* Repo owner or admin
* Other community or team contact